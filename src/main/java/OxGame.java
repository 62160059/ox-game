import java.util.Scanner;
public class OxGame {


    public static void main(String[] args) {

        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();

            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();

        
    }
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };

    static void showWelcome() {
        System.out.println("Welcome to OX game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table.length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println(" ");
        }
    }
    static char player = 'X';

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please inupt Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            System.out.println("row " + row + "col: " + col);
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty");
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != player) {
                return;
            }
           
        }
        isFinish = true;
        winner = player;
    }

    static void checkX2() {
    	int R = 2;
         for (int row = 0; row < 3; row++,R--) {
            if (table[row][R] != player) {
                return;
            }
           
        }
         isFinish = true;
         winner = player;
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkX2();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if(winner == '-'){
            System.out.println("Draw!!");
        }else{
        	showTable();
            System.out.println(winner + " Win!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye...");
    }

    static void Call(String text) {
        System.out.println(text);
    }





}
